# ML_related



## Description

Machine learning, deep learning, and visualization related technology demonstration.

## TITANIC

This project demonstrates how machine learning can be used to predict survival on the Titanic using the Titanic dataset. The Titanic dataset is a well-known dataset in the machine learning community and contains information on passengers who were aboard the Titanic when it sank, including whether they survived or not. The goal of this project is to build a machine learning model that can predict whether a passenger survived or not based on various features such as age, sex, ticket class, etc.

Demonstrating data visualization, feature selection, and predict passenger survival

## DQN

Deep Q-Learning (DQL) is a reinforcement learning algorithm based on deep neural networks that can be used to train intelligent agents to achieve specific goals. In DQL, the agent needs to learn in a virtual environment by interacting with the environment and receiving feedback signals to learn the appropriate behavior. This program demonstrates how DQL can be used to train AI agents in games and optimize decision-making at every step.


## Breast Cancer Prediction

The Breast Cancer Wisconsin (Diagnostic) Data Set is a commonly used medical data set that collects data on diagnostic breast lumps. The data set contains 569 samples, each with 32 features, including 30 real-valued features and 2 categorical features for ID and diagnosis results. The goal of this data set is to diagnose a patient's breast lump as benign or malignant, which is a binary classification problem.

Demonstrate how to use machine learning to predict whether a breast lump is benign or malignant.

## Movie Review Prediction

This project demonstrates how to use the BERT model to perform sentiment analysis prediction on the aclImdb dataset. The aclImdb dataset consists of 50,000 movie reviews from the internet movie database, with 25,000 reviews being positive and the other 25,000 reviews being negative.

Google's BERT model, which is a natural language processing model based on Transformer network architecture, was used in this project. The pre-trained BERT model on large corpora was used and fine-tuned on the aclImdb dataset to obtain a model capable of predicting sentiment analysis on new movie reviews.


## CycleGAN

A GAN consists of at least two neural networks: a generator model and a discriminator model. The generator is a neural network that creates the images.CycleGAN is a deep learning model designed for image style transfer, which consists of two generators and two discriminators.  One generator transforms images from the original domain to the target domain, while the other generator performs the reverse transformation. The discriminators play a role in distinguishing between generated images and real images. The provided code implements a CycleGAN model specifically for generating images with a Monet-style pattern.
