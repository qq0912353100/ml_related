# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 14:08:17 2019

@author: bryantliu
"""

import pandas as pd
import tensorflow as tf
import numpy as np
import time
import os


tf.reset_default_graph()

try:
    os.makedirs('/titanic/mod/')
except:
    pass

start = time.time()

# 读训练数据
data = pd.read_csv('train.csv')
 
#数据预处理

data['Sex'] = data['Sex'].apply(lambda s: 1 if s == 'male' else 0) #把性别从字符串类型转换为0或1数值型数据
data = data.fillna(0) #缺失字段填0
total_age = int()
for i in data['Age']:
    total_age += i
avg_age = int(total_age/len(data['Age']))

data['Age'] = data['Age'].apply(lambda a: avg_age if a == 0 else a)

# 选取特征
dataset_X = data[['Sex', 'Age', 'Pclass', 'SibSp', 'Parch', 'Fare']].as_matrix()
#字段说明：性别，年龄，客舱等级，兄弟姐妹和配偶在船数量，父母孩子在船的数量，船票价格
 
# 建立标签数据集
data['Deceased'] = data['Survived'].apply(lambda s: 1 - s)
dataset_Y = data[['Deceased', 'Survived']].as_matrix()


with tf.name_scope('placeholder'):
    xs = tf.placeholder(tf.float32,[None,6])
    ys = tf.placeholder(tf.float32,[None,2])


def add_layer(inputs, in_size, out_size, activation_function=None):
    Weights = tf.Variable(tf.truncated_normal([in_size, out_size],stddev=0.1),name='w')
    biases = tf.Variable(tf.truncated_normal([out_size],stddev=0.1),name='b')
    Wx_plus_b = tf.matmul(inputs, Weights) + biases   
    regularizer = tf.contrib.layers.l2_regularizer(0.0001, scope=None)
    regularizer = regularizer(Weights)
    if activation_function is None:
        outputs = Wx_plus_b
    else:
        outputs = activation_function(Wx_plus_b)
    return outputs,regularizer

l1,r1 = add_layer(xs, 6, 200, activation_function=tf.nn.relu)
l2,r2= add_layer(l1, 200, 2, activation_function=tf.nn.softmax)
y_pred = l2


global_steps= tf.Variable(0,trainable=False)
variable_average = tf.train.ExponentialMovingAverage(0.97,global_steps)
variable_average_op = variable_average.apply(tf.trainable_variables())
learning_rate = tf.train.exponential_decay(0.01,global_steps,20000,0.97)
tf.summary.scalar('learning_rate',learning_rate)
correct_prediction = tf.equal(tf.argmax(y_pred, 1), tf.argmax(ys, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
tf.summary.scalar('acc',accuracy)

#cross_entropy = tf.reduce_mean(- tf.reduce_sum(ys * tf.log(y_pred + 1e-10), reduction_indices=1)) 
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=y_pred,labels=ys))
loss = cross_entropy + r1 + r2
tf.summary.scalar('cross_entropy',cross_entropy)
train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss,global_steps)
train_opt = tf.group(train_step,variable_average_op)


merge = tf.summary.merge_all()
acc_ar = []

saver = tf.train.Saver()


with tf.Session() as sess:
    summery_writer = tf.summary.FileWriter('/path/to/log/4/',sess.graph)
    sess.run(tf.initialize_all_variables())
    for i in range(20000):
        feed_dict = {xs:dataset_X,ys:dataset_Y}
        summery,_,loss,acc = sess.run([merge,train_opt,cross_entropy,accuracy],feed_dict=feed_dict)
        acc_ar.append(acc)
        summery_writer.add_summary(summery,i)
        if i % 1000 == 0:
            print('After %04d steps loss = %f' % (i+1, loss))
    end = time.time()
    print('Complete, time cost = %f, best acc = %f  '%(end-start,max(acc_ar)))
    
    save_path = saver.save(sess, "/titanic/mod/model.ckpt")

    test_data =  pd.read_csv('test.csv')
    
    test_data['Sex'] = test_data['Sex'].apply(lambda s: 1 if s == 'male' else 0) #把性别从字符串类型转换为0或1数值型数据
    test_data = test_data.fillna(0) 
    test_data['Age'] = test_data['Age'].apply(lambda a: avg_age if a == 0 else a)
    X_test = test_data[['Sex', 'Age', 'Pclass', 'SibSp', 'Parch', 'Fare']]
    predictions = np.argmax(sess.run(y_pred, feed_dict={xs: X_test}), 1)
    
    submission = pd.DataFrame({
            "PassengerId": test_data["PassengerId"],
            "Survived": predictions
        })
    submission.to_csv("titanic-submission.csv", index=False)

