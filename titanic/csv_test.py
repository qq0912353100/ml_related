# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python

#load packages
import sys #access to system parameters https://docs.python.org/3/library/sys.html
print("Python version: {}". format(sys.version))

import pandas as pd #collection of functions for data processing and analysis modeled after R dataframes with SQL like features
print("pandas version: {}". format(pd.__version__))

import matplotlib #collection of functions for scientific and publication-ready visualization
print("matplotlib version: {}". format(matplotlib.__version__))

import numpy as np #foundational package for scientific computing
print("NumPy version: {}". format(np.__version__))

import scipy as sp #collection of functions for scientific computing and advance mathematics
print("SciPy version: {}". format(sp.__version__)) 

import IPython
from IPython import display #pretty printing of dataframes in Jupyter notebook
print("IPython version: {}". format(IPython.__version__)) 

import sklearn #collection of machine learning algorithms
print("scikit-learn version: {}". format(sklearn.__version__))

#misc libraries
import random
import time

import tensorflow as tf

#ignore warnings
import warnings
#warnings.filterwarnings('ignore')
#print('-'*25)

from subprocess import check_output
#print(check_output(["ls", "../input"]).decode("utf8"))

from sklearn import svm, tree, linear_model, neighbors, naive_bayes, ensemble, discriminant_analysis, gaussian_process
from xgboost import XGBClassifier

#Common Model Helpers
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from sklearn import feature_selection
from sklearn import model_selection
from sklearn import metrics

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import seaborn as sns
import os


data_raw = pd.read_csv('train.csv')
data_val  = pd.read_csv('test.csv')
data1 = data_raw.copy(deep = True)
data_cleaner = [data1, data_val]
#
#print(data_raw.info())
#
#print('Train columns with null values:\n', data1.isnull().sum())
#print("-"*10)

#print('Test/Validation columns with null values:\n', data_val.isnull().sum())
#print("-"*10)

for dataset in data_cleaner:
    dataset['Age'].fillna(dataset['Age'].median(),inplace=True)
    dataset['Embarked'].fillna(dataset['Embarked'].mode()[0], inplace = True)
    dataset['Fare'].fillna(dataset['Fare'].median(), inplace = True)
    
drop_column = ['PassengerId','Cabin', 'Ticket']
data1.drop(drop_column, axis=1, inplace = True)

for dataset in data_cleaner:
    dataset['FamilySize'] = dataset['SibSp']+dataset['Parch']
    dataset['IsAlone'] = 1
    dataset['IsAlone'].loc[dataset['FamilySize'] > 1] = 0
    dataset['Title'] = dataset['Name'].str.split(", ", expand=True)[1].str.split(".", expand=True)[0]
    dataset['FareBin'] = pd.qcut(dataset['Fare'], 4)
    dataset['AgeBin'] = pd.cut(dataset['Age'].astype(int), 5)

start_min = 10 
title_names = data1['Title'].value_counts() < start_min
data1['Title'] = data1['Title'].apply(lambda x: 'Misc' if title_names.loc[x] == True else x)
#print(data1['Title'].value_counts())
#print('-'*10)

label = LabelEncoder()

for dataset in data_cleaner:
    dataset['Sex_Code'] = label.fit_transform(dataset['Sex'])
    dataset['Embarked_Code'] = label.fit_transform(dataset['Embarked'])
    dataset['Title_Code'] = label.fit_transform(dataset['Title'])
    dataset['AgeBin_Code'] = label.fit_transform(dataset['AgeBin'])
    dataset['FareBin_Code'] = label.fit_transform(dataset['FareBin'])

Target = ['Survived']
data1_x = ['Sex','Pclass', 'Embarked', 'Title','SibSp', 'Parch', 'Age', 'Fare', 'FamilySize', 'IsAlone']
data1_x_calc = ['Sex_Code','Pclass', 'Embarked_Code', 'Title_Code','SibSp', 'Parch', 'Age', 'Fare']
data1_xy =  Target + data1_x
#print('Original X Y: ', data1_xy, '\n')
data1_x_bin = ['Sex_Code','Pclass', 'Embarked_Code', 'Title_Code', 'FamilySize', 'AgeBin_Code', 'FareBin_Code']
data1_xy_bin = Target + data1_x_bin
#print('Bin X Y: ', data1_xy_bin, '\n')

data1_dummy = pd.get_dummies(data1[data1_x])
data1_x_dummy = data1_dummy.columns.tolist()
data1_xy_dummy = Target + data1_x_dummy
#print('Dummy X Y: ', data1_xy_dummy, '\n')

#train1_x, test1_x, train1_y, test1_y = model_selection.train_test_split(data1[data1_x_calc], data1[Target], random_state = 0)
#train1_x_bin, test1_x_bin, train1_y_bin, test1_y_bin = model_selection.train_test_split(data1[data1_x_bin], data1[Target] , random_state = 0)
#train1_x_dummy, test1_x_dummy, train1_y_dummy, test1_y_dummy = model_selection.train_test_split(data1_dummy[data1_x_dummy], data1[Target], random_state = 0)


#print("Data1 Shape: {}".format(data1.shape))
##print("Train1 Shape: {}".format(train1_x.shape))
##print("Test1 Shape: {}".format(test1_x.shape))


#for x in data1_x:
#    if data1[x].dtype != 'float64' :
#        print('Survival Correlation by:', x)
#        print(data1[[x, Target[0]]].groupby(x, as_index=False).mean())
#        print('-'*10, '\n')
        
#print(pd.crosstab(data1['Title'],data1[Target[0]]))



#plt.figure(figsize=[16,12])## 使用plt.figure定义一个图像窗口.
#plt.subplot(231)## 多合一顯示
#plt.boxplot(x=data1['Fare'], showmeans = True, meanline = True)
#plt.title('Fare Boxplot')
#plt.ylabel('Fare ($)')
#
#plt.subplot(232)
#plt.boxplot(data1['Age'], showmeans = True, meanline = True)
#plt.title('Age Boxplot')
#plt.ylabel('Age (Years)')
#
#plt.subplot(233)
#plt.boxplot(data1['FamilySize'], showmeans = True, meanline = True)
#plt.title('Family Size Boxplot')
#plt.ylabel('Family Size (#)')
#
#plt.subplot(234)
#plt.hist(x = [data1[data1['Survived']==1]['Fare'], data1[data1['Survived']==0]['Fare']], 
#        stacked=True, color = ['g','r'],label = ['Survived','Dead'])
#plt.title('Fare Histogram by Survival')
#plt.xlabel('Fare ($)')
#plt.ylabel('# of Passengers')
#plt.legend(loc='upper right')##legend将要显示的信息来自于上面代码中的 label. 所以我们只需要简单写下一下代码, plt 就能自动的为我们添加图例.
#
#plt.subplot(235)
#plt.hist(x = [data1[data1['Survived']==1]['Age'], data1[data1['Survived']==0]['Age']], 
#         stacked=True, color = ['g','r'],label = ['Survived','Dead'])
#plt.title('Age Histogram by Survival')
#plt.xlabel('Age (Years)')
#plt.ylabel('# of Passengers')
#plt.legend(loc='upper right')
#
#plt.subplot(236)
#plt.hist(x = [data1[data1['Survived']==1]['FamilySize'], data1[data1['Survived']==0]['FamilySize']], 
#         stacked=True, color = ['g','r'],label = ['Survived','Dead'])
#plt.title('Family Size Histogram by Survival')
#plt.xlabel('Family Size (#)')
#plt.ylabel('# of Passengers')
#plt.legend(loc='upper right')
#
#fig, saxis = plt.subplots(2, 3,figsize=(16,12)) ##先定義圖顯示區域及張數
#
#sns.barplot(x = 'Embarked', y = 'Survived', data=data1, ax = saxis[0,0])
#sns.barplot(x = 'Pclass', y = 'Survived', order=[1,2,3], data=data1, ax = saxis[0,1])
#sns.barplot(x = 'IsAlone', y = 'Survived', order=[1,0], data=data1, ax = saxis[0,2])
#
#sns.pointplot(x = 'FareBin', y = 'Survived',  data=data1, ax = saxis[1,0])
#sns.pointplot(x = 'AgeBin', y = 'Survived',  data=data1, ax = saxis[1,1])
#sns.pointplot(x = 'FamilySize', y = 'Survived', data=data1, ax = saxis[1,2])
#
#fig, (axis1,axis2,axis3) = plt.subplots(1,3,figsize=(14,12))
#
#sns.boxplot(x = 'Pclass', y = 'Fare', hue = 'Survived', data = data1, ax = axis1)
#axis1.set_title('Pclass vs Fare Survival Comparison')
#
#sns.violinplot(x = 'Pclass', y = 'Age', hue = 'Survived', data = data1, split = True, ax = axis2)
#axis2.set_title('Pclass vs Age Survival Comparison')
#
#sns.boxplot(x = 'Pclass', y ='FamilySize', hue = 'Survived', data = data1, ax = axis3)
#axis3.set_title('Pclass vs Family Size Survival Comparison')
#
#fig, qaxis = plt.subplots(1,3,figsize=(14,12))
#
#sns.barplot(x = 'Sex', y = 'Survived', hue = 'Embarked', data=data1, ax = qaxis[0])
#axis1.set_title('Sex vs Embarked Survival Comparison')
#
#sns.barplot(x = 'Sex', y = 'Survived', hue = 'Pclass', data=data1, ax  = qaxis[1])
#axis1.set_title('Sex vs Pclass Survival Comparison')
#
#sns.barplot(x = 'Sex', y = 'Survived', hue = 'IsAlone', data=data1, ax  = qaxis[2])
#axis1.set_title('Sex vs IsAlone Survival Comparison')
#
#
#fig, (maxis1, maxis2) = plt.subplots(1, 2,figsize=(14,12))
#
##how does family size factor with sex & survival compare
#sns.pointplot(x="FamilySize", y="Survived", hue="Sex", data=data1,
#              palette={"male": "blue", "female": "pink"},
#              markers=["*", "o"], linestyles=["-", "--"], ax = maxis1)
#
##how does class factor with sex & survival compare
#sns.pointplot(x="Pclass", y="Survived", hue="Sex", data=data1,
#              palette={"male": "blue", "female": "pink"},
#              markers=["*", "o"], linestyles=["-", "--"], ax = maxis2)
#
#
#e = sns.FacetGrid(data1, col = 'Embarked')##選圖表內的值
#e.map(sns.pointplot, 'Pclass', 'Survived', 'Sex', ci=95.0, palette = 'deep')
#e.add_legend()
#
#a = sns.FacetGrid( data1, hue = 'Survived', aspect=4 )
#a.map(sns.kdeplot, 'Age', shade= True,cbar=True )
#a.set(xlim=(0 , data1['Age'].max()))
#a.add_legend()
#
#
##histogram comparison of sex, class, and age by survival
#h = sns.FacetGrid(data1, row = 'Sex', col = 'Pclass', hue = 'Survived')
#h.map(plt.hist, 'Age', alpha = .75)
#h.add_legend()
#
##correlation heatmap of dataset
#def correlation_heatmap(df):
#    _ , ax = plt.subplots(figsize =(14, 12))
#    colormap = sns.diverging_palette(220, 10, as_cmap = True)
#    
#    _ = sns.heatmap(
#        df.corr(), 
#        cmap = colormap,
#        square=True, 
#        cbar_kws={'shrink':.9 }, 
#        ax=ax,
#        annot=True, 
#        linewidths=0.1,vmax=1.0, linecolor='white',
#        annot_kws={'fontsize':12 }
#    )
#    
#    plt.title('Pearson Correlation of Features', y=1.05, size=15)
#
#correlation_heatmap(data1)

data1['Deceased']=data1['Survived'].apply(lambda s : 1 - s)
data1_label = data1[['Deceased', 'Survived']].as_matrix()

data_input = data1[data1_x_bin]
data_input_shape = data_input.shape[1]

tf.reset_default_graph()

try:
    os.makedirs('/titanic/mod/')
except:
    pass

start = time.time()

with tf.name_scope('placeholder'):
    xs = tf.placeholder(tf.float32,[None,data_input_shape])
    ys = tf.placeholder(tf.float32,[None,2])

def add_layer(inputs, in_size, out_size, activation_function=None):
    Weights = tf.Variable(tf.truncated_normal([in_size, out_size],stddev=0.1),name='w')
    biases = tf.Variable(tf.truncated_normal([out_size],stddev=0.1),name='b')
    Wx_plus_b = tf.matmul(inputs, Weights) + biases   
    regularizer = tf.contrib.layers.l2_regularizer(0.0001, scope=None)
    regularizer = regularizer(Weights)
    if activation_function is None:
        outputs = Wx_plus_b
    else:
        outputs = activation_function(Wx_plus_b)
    return outputs,regularizer

l1,r1 = add_layer(xs, data_input_shape, 200, activation_function=tf.nn.relu)
l2,r2= add_layer(l1, 200, 2, activation_function=tf.nn.softmax)
y_pred = l2
global_steps= tf.Variable(0,trainable=False)
variable_average = tf.train.ExponentialMovingAverage(0.97,global_steps)
variable_average_op = variable_average.apply(tf.trainable_variables())
learning_rate = tf.train.exponential_decay(0.01,global_steps,20000,0.97)
tf.summary.scalar('learning_rate',learning_rate)
correct_prediction = tf.equal(tf.argmax(y_pred, 1), tf.argmax(ys, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
tf.summary.scalar('acc',accuracy)

#cross_entropy = tf.reduce_mean(- tf.reduce_sum(ys * tf.log(y_pred + 1e-10), reduction_indices=1)) 
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=y_pred,labels=ys))
loss = cross_entropy + r1 + r2
tf.summary.scalar('cross_entropy',cross_entropy)
train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss,global_steps)
train_opt = tf.group(train_step,variable_average_op)


merge = tf.summary.merge_all()
acc_ar = []

saver = tf.train.Saver()

with tf.Session() as sess:
    sess.run(tf.initialize_all_variables())
    for i in range(20000):
        feed_dict = {xs:data_input,ys:data1_label}
        summery,_,loss,acc = sess.run([merge,train_opt,cross_entropy,accuracy],feed_dict=feed_dict)
        acc_ar.append(acc)
        if i % 1000 == 0:
            print('After %04d steps loss = %f'  % (i+1,loss))
    end = time.time()
    print('Complete, time cost = %f, best acc = %f  '%(end-start,max(acc_ar)))
    
    predictions = np.argmax(sess.run(y_pred, feed_dict={xs: data_cleaner[1][data1_x_bin]}), 1)
    submission = pd.DataFrame({
            "PassengerId": data_cleaner[1]["PassengerId"],
            "Survived": predictions
        })
    submission.to_csv("titanic-submission.csv", index=False)